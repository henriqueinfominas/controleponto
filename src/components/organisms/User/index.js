import React from 'react';

import AvatarUser from 'components/moleculus/AvatarUser';
import Acoes from './Acoes';
const User = ({ avatar }) => {
	return (
		<div>
			<AvatarUser img={avatar} />
			<Acoes />
		</div>
	);
};

export default User;
