import React from 'react';

import {FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faLaptopCode} from '@fortawesome/free-solid-svg-icons';
import ButtonAcaoIcon from '../../moleculus/ButtonAcaoIcon';
const Acoes = ({ avatar }) => {
	return (
		<div>
			<ButtonAcaoIcon icon={faLaptopCode} texto="Start"/>
		
		</div>
	);
};

export default Acoes;
