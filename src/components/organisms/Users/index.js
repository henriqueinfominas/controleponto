import React from 'react';
import { connect } from 'react-redux';

import User from 'components/organisms/User';
const Users = ({ lista }) => {
	return <div>{lista.map((item, index) => <User avatar={item.avatar} key={index} />)}</div>;
};

const mapStateToProps = ({ user }) => {
	const { lista } = user;
	return { lista };
};
export default connect(mapStateToProps)(Users);
