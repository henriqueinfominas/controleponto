import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import ButtonAcao from '../../atoms/ButtonAcao';
import IconeTexto from '../../atoms/IconeTexto';
const ButtonAcaoIcon = ({ icon }) => {
	return (
		<div>
			<ButtonAcao>
				<IconeTexto icon={icon} texto="Start" />
			</ButtonAcao>
		</div>
	);
};

export default ButtonAcaoIcon;
