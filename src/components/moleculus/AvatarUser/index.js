import React from 'react';

import AvatarImg from '../../atoms/AvatarImg';
import AvatarBorder from '../../atoms/AvatarBorder';

const AvatarUser = ({ img }) => {
	return (
		<AvatarBorder>
			<AvatarImg img={img} />
		</AvatarBorder>
	);
};

export default AvatarUser;
