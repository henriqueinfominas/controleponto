import styled from 'styled-components';

import Button from '@material-ui/core/Button';

export const ButtonStyled = styled(Button)`
  && {border-radius: 3px;
  border: 0;
  color: white;
  height:${(props) => (props.height ? props.height : 'auto')};
  padding: 0 30px;
  text-transform: none;
  font-size: 20px;
  }
`;
