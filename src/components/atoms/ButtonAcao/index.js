import React from 'react';
import { ButtonStyled } from './style';

const ButtonAcao = ({ children }) => {
	return <ButtonStyled>{children}</ButtonStyled>;
};

export default ButtonAcao;
