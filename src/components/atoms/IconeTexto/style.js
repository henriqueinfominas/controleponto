import styled from 'styled-components';

export const IconeTextoContent = styled.div`
	display: flex;
	flex-direction: column;
  align-items: center;
  padding: 10px;
`;
export const TextoIcon = styled.span``;
