import React from 'react';
import { IconeTextoContent, TextoIcon } from './style';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
const IconeTexto = ({ icon, texto }) => {
	return (
		<IconeTextoContent>
			<FontAwesomeIcon icon={icon} />
			<TextoIcon>{texto}</TextoIcon>
		</IconeTextoContent>
	);
};

export default IconeTexto;
