import styled from 'styled-components';

export const AvatarImagem = styled.img`
	width: ${(props) => (props.width ? props.width : '400px')};
	height: ${(props) => (props.height ? props.height : '400px')};
	border-radius: ${(props) => (props.radius ? props.radius : '100%')};
`;
