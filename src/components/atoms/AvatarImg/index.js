import React from 'react';
import { AvatarImagem } from './style';
const AvatarImg = ({ img }) => {
	return <AvatarImagem src={img} />;
};

export default AvatarImg;
