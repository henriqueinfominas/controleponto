import React from 'react';
import { Border } from './style';
const AvatarBorder = ({ children }) => {
	return <Border>{children}</Border>;
};

export default AvatarBorder;
