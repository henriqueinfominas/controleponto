import { create } from 'apisauce';

const api = create({
	//baseURL: process.env.API_URL ? process.env.API_URL : '' + '/api',
	baseURL: 'http://localhost:8000' + '/api',
	headers: { Accept: 'application/json' }
});

export default api;
