import { createActions, createReducer } from 'reduxsauce';
export const { Types, Creators } = createActions({
	getUsers: [ 'payload' ],
	getUsersSuces: [ 'payload' ],
	
});
const INIT_STATE = {
	lista: [],
	
};

const getUsersSuces = (state = INIT_STATE, action) => ({
	...state,
	lista: action.payload
});


export default createReducer(INIT_STATE, {
	[Types.GET_USERS_SUCES]: getUsersSuces,

});
