import React, { Component } from 'react';
import 'styles/App.css';

import { connect } from 'react-redux';
import { Creators as ActionsUser } from 'ducks/User';

import Principal from 'components/pages/Principal';

class PrincipalTela extends Component {
	state = {
		open: false
	};
	UNSAFE_componentWillMount() {
		this.props.getUsers();
	}
	handleClickButton = () => {};

	render() {
		return (
			<div className="app">
				<Principal />
			</div>
		);
	}
}
const mapStateTopProps = ({}) => {
	return {};
};
const { getUsers } = ActionsUser;
export default connect(mapStateTopProps, { getUsers })(PrincipalTela);
