import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import PrincipalTela from './routes/Principal';
import 'styles/App.css';
class App extends Component {
	render() {
		return (
			<Switch>
				<Route path="/" component={PrincipalTela} />
			</Switch>
		);
	}
}

export default App;
