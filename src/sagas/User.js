import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { Creators as ActionsUser, Types } from 'ducks/User';
import api from 'api';
const getUsersReq = async () => await api.get('user/lista').then((dados) => dados).catch((error) => error);

function* getUsers() {
	try {
		const response = yield call(getUsersReq);
		if (response.ok) {
			if (response.data.resultado) {
				const listaUsers = response.data.valor;
				yield put(ActionsUser.getUsersSuces(listaUsers));
			}
		}
	} catch (error) {
		console.log('error', error);
	}
}

export function* getUsersAcao() {
	yield takeEvery(Types.GET_USERS, getUsers);
}

export default function* rootSaga() {
	yield all([ fork(getUsersAcao) ]);
}
